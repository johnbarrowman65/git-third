package com.example.config;

import com.example.annotation.ConditionalOnSystem;
import com.example.annotation.HelloWorldSelector;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * @author 吕一明
 * @公众号 码客在线
 * @since ${date} ${time}
 */
public class HelloWorldConfigSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        Map<String, Object> isLinux = importingClassMetadata.getAnnotationAttributes(HelloWorldSelector.class.getName());

        boolean linux = (boolean)isLinux.get("isLinux");

        if(linux) {
            return new String[]{HelloWorldConfig.class.getName()};
        } else {
            return new String[]{HelloWorldConfig2.class.getName()};
        }

    }
}
